import mxnet as mx
import numpy as np
import os
from PIL import Image
from scipy.misc import imresize

import pickle
from time import time
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn import decomposition
from sklearn import datasets
from skimage import io, transform


BATCH_SIZE = 16 #reduce if it not fit in your memory


def build_net():
    prefix = "../mxnet_nets/inception_v3/Inception-7"
    num_round = 1
    model1 = mx.model.FeedForward.load(prefix, num_round, ctx=mx.gpu(), numpy_batch_size=1)
    internals = model1.symbol.get_internals()
    fea_symbol = internals['flatten_output']
    extr = mx.model.FeedForward(ctx=mx.gpu(), symbol=fea_symbol, numpy_batch_size=BATCH_SIZE,
                                     arg_params=model1.arg_params, aux_params=model1.aux_params,
                                     allow_extra_params=True)

    return extr


def preprocess(path, im_size = (299, 299)):
    # load image
    img = np.array(Image.open(path).convert("RGB"))
    # print("Original Image Shape: ", img.shape)
    # we crop image from center
    short_egde = min(img.shape[:2])
    yy = int((img.shape[0] - short_egde) / 2)
    xx = int((img.shape[1] - short_egde) / 2)
    crop_img = img[yy: yy + short_egde, xx: xx + short_egde]
    # resize to 299, 299
    resized_img = transform.resize(crop_img, im_size)

    # convert to numpy.ndarray
    sample = np.asarray(resized_img) * 256
    # swap axes to make image from (299, 299, 3) to (3, 299, 299)
    sample = np.swapaxes(sample, 0, 2)
    sample = np.swapaxes(sample, 1, 2)
    # sub mean
    normed_img = sample - 128.
    normed_img /= 128.

    return normed_img

def readimgs(folder):
    included_extenstions = ['jpg', 'bmp', 'png', 'jpeg']
    file_names = [fn for fn in os.listdir(folder)
                  if any(fn.endswith(ext) for ext in included_extenstions)]

    out = []
    for i,fn in enumerate(file_names):
        img = preprocess(os.path.join(folder, fn))
        out.append(img)

        if len(out) >= BATCH_SIZE:
            batch = np.array(out)
            out = []
            yield batch

        if i == len(file_names)- 1 and len(out) > 0:
            yield np.array(out)

def imgs4features(net, folder):

    feats = []

    for batch in readimgs(folder):
        ft = net.predict(batch)
        print(ft.shape)
        for i in range(ft.shape[0]):
            feats.append(ft[i, :])

    return np.array(feats)

def plotpca():
    t0 = time()
    net = build_net()
    X1 = imgs4features(folder='../data/0', net=net)
    X2 = imgs4features(folder='../data/1', net=net)

    print(time()-t0)
    y1 = np.zeros(X1.shape[0])#['Carrot']*X1.shape[0]
    y2 = np.ones(X2.shape[0])#['Potato']*X2.shape[0]

    X = np.concatenate((X1, X2), axis=0)
    y = np.concatenate((y1, y2), axis=0)

    print(X.shape)
    print(y)

    fig = plt.figure(1, figsize=(4, 3))
    plt.clf()
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)

    plt.cla()
    pca = decomposition.PCA(n_components=3)
    pca.fit(X)
    X = pca.transform(X)

    for name, label in [('Carrot', 0), ('Potato', 1)]:
        ax.text3D(X[y == label, 0].mean(),
                  X[y == label, 1].mean() ,
                  X[y == label, 2].mean(), name,
                  horizontalalignment='center',
                  bbox=dict(alpha=2.1, edgecolor='w', facecolor='w'))


    ax.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.spectral)

    x_surf = [X[:, 0].min(), X[:, 0].max(),
              X[:, 0].min(), X[:, 0].max()]
    y_surf = [X[:, 0].max(), X[:, 0].max(),
              X[:, 0].min(), X[:, 0].min()]
    x_surf = np.array(x_surf)
    y_surf = np.array(y_surf)
    v0 = pca.transform(pca.components_[[0]])
    v0 /= v0[-1]
    v1 = pca.transform(pca.components_[[1]])
    v1 /= v1[-1]

    ax.w_xaxis.set_ticklabels([])
    ax.w_yaxis.set_ticklabels([])
    ax.w_zaxis.set_ticklabels([])

    plt.show()

if __name__ == '__main__':
    #imgs4features('../data/0')

    plotpca()